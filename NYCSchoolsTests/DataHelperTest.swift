//
//  DataHelperTest.swift
//  NYCSchoolsTests
//
//  Created by Michael Chen on 5/29/21.
//

import XCTest
@testable import NYCSchools

class DataHelperTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testParsingSchoolData() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let schools = loadCSV(from: "2017_DOE_High_School_Directory")
        print("number of school: ----------------- \(schools.count)")
        XCTAssertTrue(!schools.isEmpty)
        
    }
    
    
    func testParsingSchoolTestData() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let schools = loadTestStatCSV(from: "2012_SAT_Results")
        print("number of school: ----------------- \(schools.count)")
        XCTAssertTrue(!schools.isEmpty)
        
    }


}
