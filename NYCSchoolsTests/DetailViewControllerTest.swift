//
//  DetailViewControllerTest.swift
//  NYCSchoolsTests
//
//  Created by Michael Chen on 5/31/21.
//

import XCTest
@testable import NYCSchools

class DetailViewControllerTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDetailViewLabels() throws {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailViewController = storyBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        //trigger view load and viewDidLoad()
        _ = detailViewController.view
        
        let dbnLabel = detailViewController.dbnLabel
        let testTakerLabel = detailViewController.testTakerLabel
        let readingLabel = detailViewController.readingLabel
        let mathLabel = detailViewController.mathLabel
        let writingLabel = detailViewController.writingLabel
        
        XCTAssertTrue(dbnLabel != nil && testTakerLabel != nil && readingLabel != nil && mathLabel != nil && writingLabel != nil)
        
    }

}
