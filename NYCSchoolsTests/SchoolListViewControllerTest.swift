//
//  SchoolListViewControllerTest.swift
//  NYCSchoolsTests
//
//  Created by Michael Chen on 5/30/21.
//

import XCTest
@testable import NYCSchools

class SchoolListViewControllerTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSortedButtonTapped() throws {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let schoolListViewController = storyBoard.instantiateViewController(withIdentifier: "SchoolListViewController") as! SchoolListViewController
        
        //trigger view load and viewDidLoad()
        _ = schoolListViewController.view
        
        //schools before sort
        let beforeSort = schoolListViewController.schools
        
        //simulating tapping sort button
        let button = schoolListViewController.sortButton!
        schoolListViewController.sortButtonTapped(button)
        
        //after button is tap schools should be sorted
        let schools = schoolListViewController.schools
        
        let afterSorted = beforeSort.sorted(by: { (school1, school2) -> Bool in
            school1.schoolName < school2.schoolName
        })
        
        //checking if the school was sorted
        XCTAssert(schools[0].schoolName.elementsEqual(afterSorted[0].schoolName))
        
    
    }
    
    
    func testSearchBarFiltering() throws {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let schoolListViewController = storyBoard.instantiateViewController(withIdentifier: "SchoolListViewController") as! SchoolListViewController
        
        //trigger view load and viewDidLoad()
        _ = schoolListViewController.view
        
        //simulating user entering text in search bar
        schoolListViewController.searchController.searchBar.text = "Avi"
        
        let filteredSchools = schoolListViewController.filteredSchools
        
        print(filteredSchools[0].schoolName)
        
        XCTAssertEqual(filteredSchools[0].schoolName, "Aviation Career & Technical Education High School")
        
    }


}
