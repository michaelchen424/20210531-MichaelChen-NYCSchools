//
//  NYCSchoolsUITests.swift
//  NYCSchoolsUITests
//
//  Created by Michael Chen on 5/29/21.
//

import XCTest

class NYCSchoolsUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSchoolTablePopulating() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
        
        let table = XCUIApplication().tables
        print("number of table cells: -------------- \(table.cells.count)")
        XCTAssert(table.cells.count > 300)

    }
    
    
    func testSelectRow() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
            
        //tapping row of table for Women's Academy of Excellence
        app.tables.staticTexts["Women's Academy of Excellence"].tap()
        
        //checking what we navigation to detail view for Women's Academy of Excellence
        let detailNavTitle = app.navigationBars["WOMEN'S ACADEMY OF EXCELLENCE"]
        XCTAssert(detailNavTitle.exists)
        
    }
    
    
    func testBackToSchoolListView() throws {
        
        let app = XCUIApplication()
        app.launch()
        
        //tapping row of table for Women's Academy of Excellence
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Women's Academy of Excellence"]/*[[".cells.staticTexts[\"Women's Academy of Excellence\"]",".staticTexts[\"Women's Academy of Excellence\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        //tapping back button
        app.navigationBars["WOMEN'S ACADEMY OF EXCELLENCE"].buttons["NYC Schools"].tap()
        
        //checking to see if we are backing the School list view
        let SchoolListNavTitle = app.navigationBars["NYC Schools"]
        XCTAssert(SchoolListNavTitle.exists)
        
    }
    

}
