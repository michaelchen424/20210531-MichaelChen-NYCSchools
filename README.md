**This application displays a list of New York High Scools and shows the SAT information for a school when selected. Users can sort the list of schools and search for the school by name or filter by borough**

<p align="center">
  <img src = NYCSchools/NYCSchoolsDemo.gif height="600">
</p>
