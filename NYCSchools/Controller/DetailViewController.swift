//
//  DetailViewController.swift
//  NYCSchools
//
//  Created by Michael Chen on 5/29/21.
//

import UIKit

class DetailViewController: UIViewController {
    
    var schoolResult: SchoolStat?
    
    @IBOutlet weak var dbnLabel: UILabel!
    
    @IBOutlet weak var testTakerLabel: UILabel!
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dbnLabel.text = "DBN: \(schoolResult?.dbn ?? "")"
        testTakerLabel.text = "Number of test takers: \(schoolResult?.numberOfTakers ?? "")"
        readingLabel.text = "Average Reading score: \(schoolResult?.readAverage ?? "")"
        mathLabel.text = "Average Math score: \(schoolResult?.mathAverage ?? "")"
        writingLabel.text = "Average Writing score: \(schoolResult?.writingAverage ?? "")"
        navigationItem.title = schoolResult?.schoolName
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
