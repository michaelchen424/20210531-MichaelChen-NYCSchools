//
//  ViewController.swift
//  NYCSchools
//
//  Created by Michael Chen on 5/29/21.
//

import UIKit

class SchoolListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortButton: UIBarButtonItem!
    
    var schools = loadCSV(from: "2017_DOE_High_School_Directory")
    
    var schoolTestStats = loadTestStatCSV(from: "2012_SAT_Results")
    
    var result: SchoolStat?
    
    //using same view that we're searching to display the result
    let searchController = UISearchController(searchResultsController: nil)
    
    var filteredSchools: [School] = []
    
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
    }
    
    @IBAction func sortButtonTapped(_ sender: UIBarButtonItem) {
        
        schools = schools.sorted(by: { (school1, school2) -> Bool in
            school1.schoolName < school2.schoolName
        })
        
        tableView.reloadData()
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
        tableView.dataSource = self
        tableView.delegate = self
        
        //lets us know of text changes in search bar
        searchController.searchResultsUpdater = self
        
        //showing result on same view controller
        searchController.obscuresBackgroundDuringPresentation = false
        
        searchController.searchBar.placeholder = "Search Schools"
        navigationItem.searchController = searchController
        
        //search bar won't remain on screen of user navigates to another view while it is active
        definesPresentationContext = true
        
        searchController.searchBar.scopeButtonTitles = School.boro.allCases.map{
            $0.rawValue
        }
        
        searchController.searchBar.delegate = self
    }
    
    
    ///filters schools base off of the search text and boro
    func filterContentforSearchText(_ searchText: String, boro: School.boro? = nil) {
                
        filteredSchools = schools.filter({ (school) -> Bool in
            
            var compare: String = ""
            
            if let boroValue = boro?.rawValue {
                //data in csv has boros as M,Q,K,X,R for Manhattan, Queens, Brooklyn, Bronx and Staten Island respectively
                switch boroValue{
                case "MN":
                    compare = "M"
                case "QN":
                    compare = "Q"
                case "BK":
                    compare = "K"
                case "BX":
                    compare = "X"
                case "SI":
                    compare = "R"
                default:
                    compare = ""
                }
                
            }
            
            //check if boro of the school matches the boro that the scope bar passes or if it is set to "All"
            let doesCatergoryMatch = boro == .All || school.schoolBoro == compare
            
            if isSearchBarEmpty {
                return doesCatergoryMatch
            }
            else {
                return doesCatergoryMatch && school.schoolName.lowercased().contains(searchText.lowercased())
            }
            
        })
        
        tableView.reloadData()
    }



}




extension SchoolListViewController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchResultsUpdating {
    
    //MARK: - Table view methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering {
            return filteredSchools.count
        }
    
        return schools.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if isFiltering {
            cell.textLabel?.text = filteredSchools[indexPath.row].schoolName
            cell.detailTextLabel?.text = filteredSchools[indexPath.row].address
        }
        else {
            cell.textLabel?.text = schools[indexPath.row].schoolName
            cell.detailTextLabel?.text = schools[indexPath.row].address
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let dbn:String
        
        if isFiltering {
            let school = filteredSchools[indexPath.row]
            dbn = school.dbn
        }
        else {
            let school = schools[indexPath.row]
            dbn = school.dbn
        }
        
        if let found = schoolTestStats.first(where: {
            $0.dbn == dbn
        }){
            
            result = found
            //print(found.dbn)
            
            performSegue(withIdentifier: "showDetail", sender: self)
            
        }
        else {
            performSegue(withIdentifier: "showNoResult", sender: self)
        }
    }
    
    
    // MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier{
        case "showDetail":
            let detailViewController =  segue.destination as! DetailViewController
            detailViewController.schoolResult = result
            
        case "showNoResult":
            return
            
        default:
            preconditionFailure("Unexpected segue identifier")
            
        }
    }
    

    //MARK: - Search bar method
    
    ///UISearchResultsUpdating method, updates search result base on what the user typed
    func updateSearchResults(for searchController: UISearchController) {
        let searchbar = searchController.searchBar
        let boro = School.boro(rawValue: searchbar.scopeButtonTitles![searchbar.selectedScopeButtonIndex])
        filterContentforSearchText(searchbar.text!, boro: boro)
    
    }
    
    
    /// scope bar, wheneber user switches the scope in the scope bar
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        let boro = School.boro(rawValue: searchBar.scopeButtonTitles![selectedScope])
        filterContentforSearchText(searchBar.text!, boro: boro)
    }

}

