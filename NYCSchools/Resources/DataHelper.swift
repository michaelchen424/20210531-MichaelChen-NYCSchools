//
//  DataHelper.swift
//  NYCSchools
//
//  Created by Michael Chen on 5/29/21.
//

import Foundation

func loadCSV(from csvName:String) -> [School] {
    
    var csvToStruct = [School]()
    
    //locate csv file
    guard let filePath = Bundle.main.path(forResource: csvName, ofType: "csv") else {
        return []
    }
    
    //convert the contents of the file into one very long string
    var data = ""
    do{
        data = try String(contentsOfFile: filePath)
    }
    catch {
        print(error)
        return []
    }
    
    //split the long string into an array of "rows" of data. Each row is a string
    //detect "/n" then split
    
    var rows = data.components(separatedBy: "\n")
    
    //remove the first row (header rows)
    rows.removeFirst()
    
    //loop around each row and split into cloumns
    for row in rows {
        
        //print("------------------------ \(row)")
                
        //regex to split comma sperated strings, but if comma enclosed in double quotes like "Clinton School Writers & Artists, M.S. 260"
        let value = row.split(usingRegex: ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)")
        
        
        let csvColumns = value.filter { (string) -> Bool in
            string != ","
        }
                
        //print("csvColumns ------------------------------")
        //print("---------------- \(csvColumns)")
        
        if csvColumns.first != "" {
            let schoolStruct = School.init(data: csvColumns)
            csvToStruct.append(schoolStruct)
        }
        
    }
    
    return csvToStruct
    
}



func loadTestStatCSV(from csvName:String) -> [SchoolStat] {
    
    var csvToStruct = [SchoolStat]()
    
    //locate csv file
    guard let filePath = Bundle.main.path(forResource: csvName, ofType: "csv") else {
        return []
    }
    
    //convert the contents of the file into one very long string
    var data = ""
    do{
        data = try String(contentsOfFile: filePath)
    }
    catch {
        print(error)
        return []
    }
    
    //split the long string into an array of "rows" of data. Each row is a string
    //detect "/n" then split
    var rows = data.components(separatedBy: "\n")
    
    //remove the first row (header rows)
    rows.removeFirst()
    
    //loop around each row and split into cloumns
    for row in rows {
        
        let value = row.split(usingRegex: ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)")
        
        
        let csvColumns = value.filter { (string) -> Bool in
            string != ","
        }
        
//        print("------------------------------")
//        print(csvColumns)
        
        if csvColumns.first != "" {
            let schoolStatStruct = SchoolStat.init(data: csvColumns)
            csvToStruct.append(schoolStatStruct)
        }

    }
    
    return csvToStruct
    
}




//extension for to split a string using regex
extension String {
    func split(usingRegex pattern: String) -> [String] {
        let regex = try! NSRegularExpression(pattern: pattern)
        let matches = regex.matches(in: self, range: NSRange(startIndex..., in: self))
        let splits = [startIndex]
            + matches
                .map { Range($0.range, in: self)! }
                .flatMap { [ $0.lowerBound, $0.upperBound ] }
            + [endIndex]

        return zip(splits, splits.dropFirst())
            .map { String(self[$0 ..< $1])}
    }
}
