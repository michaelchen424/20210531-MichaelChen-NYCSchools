//
//  SchoolStat.swift
//  NYCSchools
//
//  Created by Michael Chen on 5/29/21.
//

import Foundation

struct SchoolStat {
    let dbn: String
    let schoolName: String
    let numberOfTakers: String
    let readAverage: String
    let mathAverage: String
    let writingAverage: String
    
    init(data: [String]) {
        dbn = data[0]
        schoolName = data[1].replacingOccurrences(of: "\"", with: "")
        numberOfTakers = data[2]
        readAverage = data[3]
        mathAverage = data[4]
        writingAverage = data[5]
    }
}

/*
 
 DBN,SCHOOL NAME,Num of SAT Test Takers,SAT Critical Reading Avg. Score,SAT Math Avg. Score,SAT Writing Avg. Score
  
 */
