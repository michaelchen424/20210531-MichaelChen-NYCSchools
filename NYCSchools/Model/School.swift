//
//  School.swift
//  NYCSchools
//
//  Created by Michael Chen on 5/29/21.
//

import Foundation

struct School {
    let dbn: String
    let schoolName: String
    let schoolBoro: String
    let address: String
    
    init(data: [String]) {
        dbn = data[0]
        schoolName = data[1].replacingOccurrences(of: "\"", with: "")
        schoolBoro = data[2]
        address = data[18].replacingOccurrences(of: "\"", with: "").components(separatedBy: "(")[0]
    }
    
    enum boro {
        case All
        case Manhattan
        case Queens
        case Brooklyn
        case Bronx
        case StatenIsland
    }
}


extension School.boro: CaseIterable { }

extension School.boro: RawRepresentable {
    typealias RawValue = String
    
    init?(rawValue: String) {
        switch rawValue {
        case "All":
            self = .All
        case "MN":
            self = .Manhattan
        case "QN":
            self = .Queens
        case "BK":
            self = .Brooklyn
        case "BX":
            self = .Bronx
        case "SI":
            self = .StatenIsland
        default: return nil
        }
    }
    
    
    var rawValue: RawValue {
      switch self {
      case .All: return "All"
      case .Manhattan: return "MN"
      case .Queens: return "QN"
      case .Brooklyn: return "BK"
      case .Bronx: return "BX"
      case .StatenIsland: return "SI"
      }
    }
}
